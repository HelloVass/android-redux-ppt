# 构建须知

## PPT 基于 reveal.js 生成
* [关于 reveal.js](https://github.com/hakimel/reveal.js/)
* reveal.js 通过读取根目录下的 share.md 生成 PPT

## 构建步骤

1. clone 该项目
> git clone git@gitlab.com:HelloVass/android-redux-ppt.git

2. 进入该项目
> cd android-redux-ppt

3. 安装依赖
> install npm

4. 开启 NPM
> npm start

5. 体验船新的 PPT
> Open http://localhost:8000 to view your presentation

## reveal.js 小技巧（可选）

### 注意 index.html 内的这段代码

```html
<div class="reveal">
			<div class="slides">
				<section data-markdown="share.md" data-separator="___"
					data-separator-vertical="__" data-separator-notes="^Note:"
					data-charset="UTF-8">
				</section>
			</div>
		</div>
```

`data-separator` 设置横向分页，`data-separator-vertical` 设置纵向分页。

### 开启历史记录
```javascript
Reveal.initialize({
				dependencies: [
					{ src: 'plugin/markdown/marked.js' },
					{ src: 'plugin/markdown/markdown.js' },
					{ src: 'plugin/notes/notes.js', async: true },
					{ src: 'plugin/highlight/highlight.js', async: true, callback: function() { hljs.initHighlightingOnLoad(); } }
				],
				history:true
			});
```


