# `Redux是怎样炼成的`

## 分享者
- 政企互联部移动端 Android 开发[陈 轲(HelloVass)](https://hellovass.info/)
___
## 小前端
___
## 小前端是啥
> 熟练掌握原生或者 WEB，学习其他端的知识，尽最大可能整合不同端的优势并且落地到项目中的开发者
__
## 小前端面临的挑战
__
## `RN&WEEX`
* React 知识体系
* Vue 知识体系
* Native 知识体系
__
## `WebView`
* Native->JS 通信&JS->Native通信
* 统一的 WebView Bridge
__
## `Flutter`
* Dart VM
* React 知识体系
___
## 小前端在忙啥
> UI = render(data)

* UI = 用户看到的界面
* data = 渲染的数据
__
## 组件化之后的困惑
![组件状态不一致](https://hellovass-blog-1257365569.cos.ap-shanghai.myqcloud.com/%E7%BB%84%E4%BB%B6%E5%8C%96%E4%B9%8B%E5%90%8E%E7%8A%B6%E6%80%81%E4%B8%8D%E4%B8%80%E8%87%B4.jpg)

___
## `从Flux到Redux`

__
## `MVC 的缺点`
![](https://hellovass-blog-1257365569.cos.ap-shanghai.myqcloud.com/mvc%E6%A1%86%E6%9E%B6.png)

__
## `Flux提倡的单向数据流`
![](https://hellovass-blog-1257365569.cos.ap-shanghai.myqcloud.com/Flux%E5%8D%95%E9%A1%B9%E6%95%B0%E6%8D%AE%E6%B5%81.png)

__
## 相关概念
* Action， 视图层发出的消息
* View，视图层
* Dispatcher，用来接收 Action，执行回调
* Store，用来存放应用的状态，一旦发生变动，就通知 View 更新

__
## `Flux的缺点`
* store 之间存在依赖关系，需要用 `Dispatcher` 的 `waitfor` 函数解决
* 难以进行服务端的渲染

___
## `Redux` 

__
## 基本原则
*  唯一数据源(Single Source of Truth)
* 保持状态只读(State is read-only)
* 数据改变只能通过纯函数完成(Changs are made with pure functions)

__
## `Reducer+Flux=Redux`
```kotlin
interface Reducer<S> {
    fun reduce(state: S, action: Action): S
}
```

__ 
## `理想的Redux模型`
![](https://hellovass-blog-1257365569.cos.ap-shanghai.myqcloud.com/%E7%90%86%E6%83%B3%E7%9A%84%20Redux%20%E6%A8%A1%E5%9E%8B.jpg)

__ 
## 然而现实是残酷的
* 副作用不可避免

__
## `中间件(Middleware)`
![](https://hellovass-blog-1257365569.cos.ap-shanghai.myqcloud.com/redux-flow-middleware-diagram.png)


__ 
## `来看一段 js 代码`
```javascript
let next = store.dispatch;
store.dispatch = function dispatchAndLog(action) {
  console.log('dispatching', action);
  next(action);
  console.log('next state', store.getState());
}
```
我们增强了 dispatch 方法，给它添加了打印日志的的功能，这就是中间件的雏形。

___
## `Middleware模型&koa2`
* [图解 Redux 中的 middleware 洋葱圈模型](https://juejin.im/post/5adec636518825670b33b7e8)


__
## `复合函数compose`
```javascript
compose(A, B, C)(arg) === A(B(C(arg)))
```
> 从右往左依次将右边的返回值作为左边的参数传入，层层包裹

__
## `柯里化绑定`
```javascript
const store = createStore(...args)
let chain = []
const middlewareAPI = {
    getState: store.getState,
    dispatch: (...args) => dispatch(...args)
}
chain = middlewares.map(middleware => middleware(middlewareAPI)) // 绑定 {dispatch和getState}
dispatch = compose(...chain)(store.dispatch) // 绑定 next
```
___
## `Android端Redux实现`
[android-redux](https://gitlab.com/HelloVass/android-redux)

__
## `why not EventBus like?`
* EventBus
* auto
* RxBus

__
## Answer
[为什么要放弃 RxBus](https://zhuanlan.zhihu.com/p/26160377)

___ 
## 参考
* [Redux architecture for android apps](https://jayrambhia.com/blog/android-redux-intro)
* [阮一峰 Redux 教程](http://www.ruanyifeng.com/blog/2016/09/redux_tutorial_part_one_basic_usages.html)
* [图解 Redux 洋葱模型](https://juejin.im/post/5adec636518825670b33b7e8)
* [深入浅出 React 和 Redux](https://book.douban.com/subject/27033213/)
* [Kotlin 极简教程](https://www.jianshu.com/p/3c445248a5e3)
* [Kotlin 柯里化](https://blog.csdn.net/wd2014610/article/details/79795636)

___
## `Thanks`